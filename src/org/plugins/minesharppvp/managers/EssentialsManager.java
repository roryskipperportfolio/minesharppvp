package org.plugins.minesharppvp.managers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.plugins.minesharppvp.MineSharpPvPMain;

import java.io.File;
import java.io.IOException;

public class EssentialsManager {

	private final MineSharpPvPMain plugin;
	private final SpawnpointManager spawnpointManager;

	public EssentialsManager(MineSharpPvPMain plugin, SpawnpointManager spawnpointManager) {
		this.plugin = plugin;
		this.spawnpointManager = spawnpointManager;
	}

	public void disableGodmode(Player p) {
		File file = new File(this.plugin.getDataFolder().getParentFile() + File.separator + "Essentials" + File.separator + "userdata" + File.separator + p.getUniqueId().toString() + ".yml");
		if (file.exists()) {
			FileConfiguration conf = YamlConfiguration.loadConfiguration(file);
			if (conf.getBoolean("godmode", false)) {
				conf.set("godmode", false);
				p.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.god-disabled")));
				this.plugin.getServer().dispatchCommand(this.plugin.getServer().getConsoleSender(), "ess reload");
				try {
					conf.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void disableFly(Player p) {
		if (p.getAllowFlight()) {
			p.setAllowFlight(false);
			p.setFlying(false);
			p.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.fly-disabled")));
		}
	}

	public void disablePowertools(Player p) {
		File file = new File(this.plugin.getDataFolder().getParentFile() + File.separator + "Essentials" + File.separator + "userdata" + File.separator + p.getUniqueId().toString() + ".yml");
		if (file.exists()) {
			FileConfiguration conf = YamlConfiguration.loadConfiguration(file);
			if (conf.isSet("powertools")) {
				conf.set("powertools", null);
				p.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.powertools-cleared")));
				this.plugin.getServer().dispatchCommand(this.plugin.getServer().getConsoleSender(), "ess reload");
				try {
					conf.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void resetSpeed(Player p) {
		if (p.getWalkSpeed() != 0.2f || p.getFlySpeed() != 0.1f) {
			p.setWalkSpeed(0.2f);
			p.setFlySpeed(0.1f);
		}
	}

	public boolean backLocationInPvPWorld(Player p) {
		File file = new File(this.plugin.getDataFolder().getParentFile() + File.separator + "Essentials" + File.separator + "userdata" + File.separator + p.getUniqueId().toString() + ".yml");
		if (file.exists()) {
			FileConfiguration conf = YamlConfiguration.loadConfiguration(file);
			if (conf.isSet("lastlocation")) {
				if (this.plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(conf.getString("lastlocation.world"))) {
					Location loc = new Location(Bukkit.getWorld(conf.getString("lastlocation.world")), conf.getDouble("lastlocation.x"), conf.getDouble("lastlocation.y"), conf.getDouble("lastlocation.z"),
							Float.valueOf(conf.getString("lastlocation.yaw")), Float.valueOf(conf.getString("lastlocation.pitch")));
					if (this.spawnpointManager.inSpawnpoint(loc)) {
						return false;
					}
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
