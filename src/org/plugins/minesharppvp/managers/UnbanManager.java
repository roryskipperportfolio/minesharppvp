package org.plugins.minesharppvp.managers;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.scheduler.BukkitTask;

public class UnbanManager {

	private HashMap<UUID, BukkitTask> unbanTasks = new HashMap<UUID, BukkitTask>();

	public void cancelUnbanTask(UUID uuid) {
		if (this.unbanTasks.containsKey(uuid)) {
			this.unbanTasks.get(uuid).cancel();
		}
	}

	public void removeUnbanTask(UUID uuid) {
		if (this.unbanTasks.containsKey(uuid)) {
			this.unbanTasks.remove(uuid);
		}
	}

	public void addUnbanTask(UUID uuid, BukkitTask task) {
		this.unbanTasks.put(uuid, task);
	}
	
	public boolean hasUnbanTask(UUID uuid) {
		return this.unbanTasks.containsKey(uuid);
	}
	
	public void cancelAllTasks() {
		for (BukkitTask value : this.unbanTasks.values()) {
			value.cancel();
		}
	}

}
