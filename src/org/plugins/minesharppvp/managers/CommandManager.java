package org.plugins.minesharppvp.managers;

import java.util.HashMap;
import java.util.UUID;

import org.plugins.minesharppvp.util.DelayedCommand;

public class CommandManager {
	
	private HashMap<UUID, DelayedCommand> commandTasks = new HashMap<UUID, DelayedCommand>();

	public void cancelCommandTask(UUID uuid) {
		if (this.commandTasks.containsKey(uuid)) {
			this.commandTasks.get(uuid).cancel();
		}
	}
	
	public void removeCommandTask(UUID uuid) {
		if (this.commandTasks.containsKey(uuid)) {
			this.commandTasks.remove(uuid);
		}
	}

	public void addCommandTask(UUID uuid, DelayedCommand delayedCommand) {
		this.commandTasks.put(uuid, delayedCommand);
	}
	
	public boolean hasCommandTask(UUID uuid) {
		return this.commandTasks.containsKey(uuid);
	}
	
	public DelayedCommand getDelayedCommand(UUID uuid) {
		return this.commandTasks.containsKey(uuid) ? this.commandTasks.get(uuid) : null;
	}
	
	public void cancelAllTasks() {
		for (DelayedCommand value : this.commandTasks.values()) {
			value.cancel();
		}
	}
}
