package org.plugins.minesharppvp.managers;

import java.util.HashSet;
import java.util.UUID;

public class DeathManager {

	HashSet<UUID> forceKilled =  new HashSet<UUID>();
	
	public void addKilled(UUID uuid) {
		this.forceKilled.add(uuid);
	}
	
	public boolean wasForceKilled(UUID uuid) {
		return this.forceKilled.contains(uuid);
	}
	
	public void removeKilled(UUID uuid) {
		this.forceKilled.remove(uuid);
	}
}
