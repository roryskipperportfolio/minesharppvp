package org.plugins.minesharppvp.managers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.objects.Spawnpoint;

import java.util.HashMap;

public class SpawnpointManager {

    private final MineSharpPvPMain plugin;
    HashMap<World, Spawnpoint> spawnpoints = new HashMap<>();

    public SpawnpointManager(MineSharpPvPMain plugin) {
        this.plugin = plugin;
    }

    public void loadSpawnpoints() {
        for (String worldName : this.plugin.getArenaConfig().getConfig().getConfigurationSection("worlds").getKeys(false)) {
            World world = Bukkit.getWorld(worldName);
            if (world != null) {
                this.refreshSpawnpoint(world);
            }
        }
    }

    public void refreshSpawnpoint(World world) {
        this.spawnpoints.remove(world);
        String worldName = world.getName();
        int maxX = this.plugin.getArenaConfig().getConfig().getInt("worlds." + worldName + ".spawnpoint.corner1.x");
        int maxY = this.plugin.getArenaConfig().getConfig().getInt("worlds." + worldName + ".spawnpoint.corner1.y");
        int maxZ = this.plugin.getArenaConfig().getConfig().getInt("worlds." + worldName + ".spawnpoint.corner1.z");
        int minX = this.plugin.getArenaConfig().getConfig().getInt("worlds." + worldName + ".spawnpoint.corner2.x");
        int minY = this.plugin.getArenaConfig().getConfig().getInt("worlds." + worldName + ".spawnpoint.corner2.y");
        int minZ = this.plugin.getArenaConfig().getConfig().getInt("worlds." + worldName + ".spawnpoint.corner2.z");
        this.spawnpoints.put(world, new Spawnpoint(world, maxX, maxY, maxZ, minX, minY, minZ));
    }

    public void removeSpawnpoint(World world) {
        this.plugin.getArenaConfig().getConfig().set("worlds." + world.getName(), null);
        this.plugin.getArenaConfig().saveConfig();
        this.plugin.getArenaConfig().reloadConfig();
        this.spawnpoints.remove(world);
    }

    public boolean inSpawnpoint(Location loc) {
        if (this.spawnpoints.containsKey(loc.getWorld())) {
            return this.spawnpoints.get(loc.getWorld()).inSpawnpoint(loc);
        }
        return false;
    }

}
