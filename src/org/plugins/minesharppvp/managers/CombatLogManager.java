package org.plugins.minesharppvp.managers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.iiSnipez.CombatLog.CombatLog;

public class CombatLogManager {

	private CombatLog combatLog = null;

	public void setupCombatLog() {
		if (Bukkit.getPluginManager().isPluginEnabled("CombatLog")) {
			this.combatLog = (CombatLog) Bukkit.getPluginManager().getPlugin("CombatLog");
		}
	}

	public boolean isTagged(Player p) {
		return this.combatLog == null ? false : this.combatLog.taggedPlayers.containsKey(p.getName());
	}

}
