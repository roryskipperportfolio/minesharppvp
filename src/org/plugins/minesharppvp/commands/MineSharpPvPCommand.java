package org.plugins.minesharppvp.commands;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.DeathManager;
import org.plugins.minesharppvp.managers.SpawnpointManager;
import org.plugins.minesharppvp.managers.UnbanManager;

import java.util.UUID;

public class MineSharpPvPCommand implements CommandExecutor {

	private final MineSharpPvPMain plugin;
	private final SpawnpointManager spawnpointManager;
	private final UnbanManager unbanManager;
	private final DeathManager deathManager;

	public MineSharpPvPCommand(MineSharpPvPMain plugin, SpawnpointManager spawnpointManager, UnbanManager unbanManager, DeathManager deathManager) {
		this.plugin = plugin;
		this.spawnpointManager = spawnpointManager;
		this.unbanManager = unbanManager;
		this.deathManager = deathManager;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("minesharppvp")) {

			if (args.length > 0) {

				if (args[0].equalsIgnoreCase("reload")) {
					if (!sender.hasPermission("mspvp.reload")) {
						sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.no-permission")));
						return false;
					}

					this.plugin.reloadConfig();
					this.plugin.getArenaConfig().reloadConfig();
					this.plugin.getPlayersConfig().reloadConfig();
					sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.config-reloaded")));
					return true;
				}

				if (args[0].equalsIgnoreCase("spawnpoint")) {

					if (args.length > 1) {

						if (!(sender instanceof Player)) {
							sender.sendMessage(this.plugin.placeholders("{PREFIX}You can only use this command in-game"));
							return false;
						}

						Player p = (Player) sender;
						World world = p.getWorld();
						String worldName = world.getName();

						if (!this.plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(world.getName())) {
							sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.not-in-pvp-world")));
							return false;
						}

						if (args[1].equalsIgnoreCase("clear")) {
							if (!sender.hasPermission("mspvp.spawnpoint.clear")) {
								sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.no-permission")));
								return false;
							}

							this.spawnpointManager.removeSpawnpoint(world);
							sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.spawnpoint-cleared")));
							return true;
						}

						if (args[1].equalsIgnoreCase("set")) {
							if (!sender.hasPermission("mspvp.spawnpoint.set")) {
								sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.no-permission")));
								return false;
							}
							if (!Bukkit.getServer().getPluginManager().isPluginEnabled("WorldEdit")) {
								sender.sendMessage(this.plugin.placeholders("{PREFIX}You need WorldEdit enabled to use this command"));
								return false;
							}

							WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
							Selection selection = worldEdit.getSelection((Player) sender);

							if (selection == null) {
								sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.no-selection")));
								return false;
							}

							Location corner1 = selection.getMaximumPoint();
							Location corner2 = selection.getMinimumPoint();
							this.plugin.getArenaConfig().getConfig().set("worlds." + worldName + ".spawnpoint.corner1.x", corner1.getBlockX());
							this.plugin.getArenaConfig().getConfig().set("worlds." + worldName + ".spawnpoint.corner1.y", corner1.getBlockY());
							this.plugin.getArenaConfig().getConfig().set("worlds." + worldName + ".spawnpoint.corner1.z", corner1.getBlockZ());
							this.plugin.getArenaConfig().getConfig().set("worlds." + worldName + ".spawnpoint.corner2.x", corner2.getBlockX());
							this.plugin.getArenaConfig().getConfig().set("worlds." + worldName + ".spawnpoint.corner2.y", corner2.getBlockY());
							this.plugin.getArenaConfig().getConfig().set("worlds." + worldName + ".spawnpoint.corner2.z", corner2.getBlockZ());
							this.plugin.getArenaConfig().saveConfig();
							this.plugin.getArenaConfig().reloadConfig();
							this.spawnpointManager.refreshSpawnpoint(world);
							sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.spawnpoint-set")));
							return true;
						}

					}

				}

				if (args[0].equalsIgnoreCase("ban")) {
					if (!sender.hasPermission("mspvp.ban")) {
						sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.no-permission")));
						return false;
					}

					if (args.length > 1) {

						if (args.length > 2) {

							String playerName = "";
							final UUID uuid;
							if (Bukkit.getPlayer(args[1]) != null) {
								playerName = Bukkit.getPlayer(args[1]).getName();
								uuid = Bukkit.getPlayer(args[1]).getUniqueId();
							} else if (Bukkit.getOfflinePlayer(args[1]) != null) {
								if (!Bukkit.getOfflinePlayer(args[1]).hasPlayedBefore()) {
									sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.never-played-before").replace("{PLAYER}", args[1])));
									return false;
								} else {
									playerName = Bukkit.getOfflinePlayer(args[1]).getName();
									uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId();
								}
							} else {
								sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.never-played-before").replace("{PLAYER}", args[1])));
								return false;
							}

							if (this.plugin.getPlayersConfig().getConfig().isSet("banned-players." + uuid.toString()) || this.plugin.getPlayersConfig().getConfig().isSet("temp-banned-players." + uuid.toString())) {
								sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.already-banned").replace("{PLAYER}", playerName)));
								return false;
							}

							String time = "";
							for (int index = 2; index < args.length; index++) {
								time += args[index];
							}

							long seconds = this.convertToSeconds(time);

							Long currentTime = System.currentTimeMillis();
							this.plugin.getPlayersConfig().getConfig().set("temp-banned-players." + uuid.toString() + ".banner", sender instanceof Player ? ((Player) sender).getName() : sender.getName());
							this.plugin.getPlayersConfig().getConfig().set("temp-banned-players." + uuid.toString() + ".ban-date", currentTime);
							this.plugin.getPlayersConfig().getConfig().set("temp-banned-players." + uuid.toString() + ".ban-length", sender instanceof Player ? ((Player) sender).getName() : sender.getName());
							this.plugin.getPlayersConfig().getConfig().set("temp-banned-players." + uuid.toString() + ".unban-date", currentTime + (long) (seconds * 1000));
							this.plugin.getPlayersConfig().saveConfig();
							this.plugin.getPlayersConfig().reloadConfig();
							String formattedTime = this.formatTime(seconds);
							this.unbanManager.addUnbanTask(uuid, new BukkitRunnable() {
								@Override
								public void run() {
									plugin.getPlayersConfig().getConfig().set("temp-banned-players." + uuid, null);
									plugin.getPlayersConfig().saveConfig();
									plugin.getPlayersConfig().reloadConfig();
									if (Bukkit.getPlayer(uuid) != null) {
										Bukkit.getPlayer(uuid).sendMessage(plugin.placeholders(plugin.getConfig().getString("messages.your-unbanned")));
									}
								}
							}.runTaskLater(this.plugin, 20L * ((this.plugin.getPlayersConfig().getConfig().getLong("temp-banned-players." + uuid + ".unban-date") - System.currentTimeMillis()) / 1000)));
							sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.player-temp-banned").replace("{PLAYER}", playerName).replace("{TIME}", formattedTime)));
							if (Bukkit.getPlayer(uuid) != null) {
								Bukkit.getPlayer(uuid).sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.your-temp-banned").replace("{TIME}", formattedTime).replace("{BANNER}",
										sender instanceof Player ? ((Player) sender).getName() : sender.getName())));
								this.deathManager.addKilled(uuid);
								Bukkit.getPlayer(uuid).damage(Short.MAX_VALUE);
								if (Bukkit.getPlayer(uuid).getHealth() > 0) {
									Bukkit.getPlayer(uuid).setHealth(0);
								}
							}
							return true;
						}

						String playerName = "";
						UUID uuid = null;
						if (Bukkit.getPlayer(args[1]) != null) {
							playerName = Bukkit.getPlayer(args[1]).getName();
							uuid = Bukkit.getPlayer(args[1]).getUniqueId();
						} else if (Bukkit.getOfflinePlayer(args[1]) != null) {
							if (!Bukkit.getOfflinePlayer(args[1]).hasPlayedBefore()) {
								sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.never-played-before").replace("{PLAYER}", args[1])));
								return false;
							} else {
								playerName = Bukkit.getOfflinePlayer(args[1]).getName();
								uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId();
							}
						} else {
							sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.never-played-before").replace("{PLAYER}", args[1])));
							return false;
						}

						if (this.plugin.getPlayersConfig().getConfig().isSet("banned-players." + uuid.toString()) || this.plugin.getPlayersConfig().getConfig().isSet("temp-banned-players." + uuid.toString())) {
							sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.already-banned").replace("{PLAYER}", playerName)));
							return false;
						}

						this.plugin.getPlayersConfig().getConfig().set("banned-players." + uuid.toString() + ".banner", sender instanceof Player ? ((Player) sender).getName() : sender.getName());
						this.plugin.getPlayersConfig().getConfig().set("banned-players." + uuid.toString() + ".ban-date", System.currentTimeMillis());
						this.plugin.getPlayersConfig().saveConfig();
						this.plugin.getPlayersConfig().reloadConfig();
						sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.player-banned").replace("{PLAYER}", playerName)));
						if (Bukkit.getPlayer(uuid) != null) {
							Bukkit.getPlayer(uuid).sendMessage(this.plugin
									.placeholders(this.plugin.getConfig().getString("messages.your-banned").replace("{BANNER}", sender instanceof Player ? ((Player) sender).getName() : sender.getName())));
							this.deathManager.addKilled(uuid);
							Bukkit.getPlayer(uuid).damage(Short.MAX_VALUE);
							if (Bukkit.getPlayer(uuid).getHealth() > 0) {
								Bukkit.getPlayer(uuid).setHealth(0);
							}
						}
						return true;
					}

				}

				if (args[0].equalsIgnoreCase("unban")) {
					if (!sender.hasPermission("mspvp.unban")) {
						sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.no-permission")));
						return false;
					}

					String playerName = "";
					UUID uuid = null;
					if (Bukkit.getPlayer(args[1]) != null) {
						playerName = Bukkit.getPlayer(args[1]).getName();
						uuid = Bukkit.getPlayer(args[1]).getUniqueId();
					} else if (Bukkit.getOfflinePlayer(args[1]) != null) {
						if (!Bukkit.getOfflinePlayer(args[1]).hasPlayedBefore()) {
							sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.never-played-before").replace("{PLAYER}", args[1])));
							return false;
						} else {
							playerName = Bukkit.getOfflinePlayer(args[1]).getName();
							uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId();
						}
					} else {
						sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.never-played-before").replace("{PLAYER}", args[1])));
						return false;
					}

					if (this.plugin.getPlayersConfig().getConfig().isSet("banned-players." + uuid.toString())) {
						this.plugin.getPlayersConfig().getConfig().set("banned-players." + uuid.toString(), null);
					} else if (this.plugin.getPlayersConfig().getConfig().isSet("temp-banned-players." + uuid.toString())) {
						this.plugin.getPlayersConfig().getConfig().set("temp-banned-players." + uuid.toString(), null);
					} else {
						sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.player-isnt-banned").replace("{PLAYER}", playerName)));
						return false;
					}
					this.plugin.getPlayersConfig().saveConfig();
					this.plugin.getPlayersConfig().reloadConfig();
					sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.player-unbanned").replace("{PLAYER}", playerName)));
					if (Bukkit.getPlayer(uuid) != null) {
						Bukkit.getPlayer(uuid).sendMessage(
								this.plugin.placeholders(this.plugin.getConfig().getString("messages.your-unbanned").replace("{BANNER}", sender instanceof Player ? ((Player) sender).getName() : sender.getName())));
					}
					if (this.unbanManager.hasUnbanTask(uuid)) {
						this.unbanManager.cancelUnbanTask(uuid);
						this.unbanManager.removeUnbanTask(uuid);
					}
					return true;

				}

				if (!sender.hasPermission("mspvp.help")) {
					sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.no-permission")));
					return false;
				}

				for (String helpLine : this.plugin.getConfig().getStringList("messages.help-message")) {
					sender.sendMessage(this.plugin.placeholders(helpLine));
				}

				return false;
			}

			if (!sender.hasPermission("mspvp.help")) {
				sender.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.no-permission")));
				return false;
			}

			for (String helpLine : this.plugin.getConfig().getStringList("messages.help-message")) {
				sender.sendMessage(this.plugin.placeholders(helpLine));
			}

			return true;
		}

		return false;

	}

	private long convertToSeconds(String time) {
		time = time.toLowerCase();
		long seconds = 0;
		int index = 0;
		while (!time.equals("")) {
			char ch = time.charAt(index);
			switch (ch) {
			case 's':
				seconds += Integer.parseInt(time.substring(0, index));
				if (time.length() > index + 1) {
					time = time.substring(index + 1);
					index = 0;
				} else {
					return seconds;
				}
				break;
			case 'm':
				if (time.charAt(index + 1 >= time.length() ? index : index + 1) == 'o') {
					seconds += Integer.parseInt(time.substring(0, index)) * 30 * 24 * 60 * 60;
					if (time.length() > index + 2) {
						time = time.substring(index + 2);
						index = 0;
					} else {
						return seconds;
					}
				} else {
					seconds += Integer.parseInt(time.substring(0, index)) * 60;
					if (time.length() > index + 1) {
						time = time.substring(index + 1);
						index = 0;
					} else {
						return seconds;
					}
				}
				break;
			case 'h':
				seconds += Integer.parseInt(time.substring(0, index)) * 60 * 60;
				if (time.length() > index + 1) {
					time = time.substring(index + 1);
					index = 0;
				} else {
					return seconds;
				}
				break;
			case 'd':
				seconds += Integer.parseInt(time.substring(0, index)) * 60 * 60 * 24;
				if (time.length() > index + 1) {
					time = time.substring(index + 1);
					index = 0;
				} else {
					return seconds;
				}
			case 'w':
				seconds += Integer.parseInt(time.substring(0, index)) * 60 * 60 * 24 * 7;
				if (time.length() > index + 1) {
					time = time.substring(index + 1);
					index = 0;
				} else {
					return seconds;
				}
			case 'y':
				seconds += Integer.parseInt(time.substring(0, index)) * 60 * 60 * 24 * 365;
				if (time.length() > index + 1) {
					time = time.substring(index + 1);
					index = 0;
				} else {
					return seconds;
				}

			}
			index++;
		}
		return seconds;
	}

	private String formatTime(long seconds) {
		String timeText = "";
		boolean firstTime = true;
		if (seconds % (60 * 60 * 24 * 365) >= 0) {
			int timecalc = (int) Math.floor(seconds / (60 * 60 * 24 * 365));
			seconds = seconds % (60 * 60 * 24 * 365);
			if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " year,";
				} else {
					timeText += String.valueOf(timecalc) + " years,";
				}
				firstTime = false;
			}
		}

		if (seconds % (30 * 24 * 60 * 60) >= 0) {
			int timecalc = (int) Math.floor(seconds / (30 * 24 * 60 * 60));
			seconds = seconds % (30 * 24 * 60 * 60);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " month,";
				} else {
					timeText += String.valueOf(timecalc) + " months,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " month,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " months,";
				}
			}
		}

		if (seconds % (60 * 60 * 24 * 7) >= 0) {
			int timecalc = (int) Math.floor(seconds / (60 * 60 * 24 * 7));
			seconds = seconds % (60 * 60 * 24 * 7);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " week,";
				} else {
					timeText += String.valueOf(timecalc) + " weeks,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " week,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " weeks,";
				}
			}
		}

		if (seconds % (60 * 60 * 24) >= 0) {
			int timecalc = (int) Math.floor(seconds / (60 * 60 * 24));
			seconds = seconds % (60 * 60 * 24);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " day,";
				} else {
					timeText += String.valueOf(timecalc) + " days,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " day,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " days,";
				}
			}
		}

		if (seconds % (60 * 60) >= 0) {
			int timecalc = (int) Math.floor(seconds / (60 * 60));
			seconds = seconds % (60 * 60);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " hour,";
				} else {
					timeText += String.valueOf(timecalc) + " hours,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " hour,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " hours,";
				}
			}
		}

		if (seconds % 60 >= 0) {
			int timecalc = (int) Math.floor(seconds / (60));
			seconds = seconds % (60);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " minute,";
				} else {
					timeText += String.valueOf(timecalc) + " minutes,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " minute,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " minutes,";
				}
			}
		}

		if (seconds > 0 && firstTime) {
			if (seconds == 1) {
				timeText += String.valueOf(seconds) + " second,";
			} else {
				timeText += String.valueOf(seconds) + " seconds,";
			}
		} else if (seconds > 0) {
			if (seconds == 1) {
				timeText += " " + String.valueOf(seconds) + " second,";
			} else {
				timeText += " " + String.valueOf(seconds) + " seconds,";
			}
		}
		timeText = timeText.substring(0, timeText.length() - 1);
		int lastComma = timeText.lastIndexOf(",");
		if (lastComma != -1) {
			timeText = timeText.substring(0, lastComma) + " and " + timeText.substring(lastComma + 2, timeText.length());
		}
		return timeText;
	}

}
