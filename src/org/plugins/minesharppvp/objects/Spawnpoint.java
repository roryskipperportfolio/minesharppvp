package org.plugins.minesharppvp.objects;

import org.bukkit.Location;
import org.bukkit.World;

public class Spawnpoint {

    World world;
    private int maxX;
    private int maxY;
    private int maxZ;
    private int minX;
    private int minY;
    private int minZ;

    public Spawnpoint(World world, int maxX, int maxY, int maxZ, int minX, int minY, int minZ) {
        this.world = world;
        this.maxX = maxX;
        this.maxY = maxY;
        this.maxZ = maxZ;
        this.minX = minX;
        this.minY = minY;
        this.minZ = minZ;
    }

    public boolean inSpawnpoint(Location loc) {
        if (this.maxX >= loc.getBlockX() && loc.getBlockX() >= this.minX && this.maxY >= loc.getBlockY()
                && loc.getBlockY() >= this.minY && this.maxZ >= loc.getBlockZ() && loc.getBlockZ() >= this.minZ) {
            return true;
        }
        return false;
    }
}
