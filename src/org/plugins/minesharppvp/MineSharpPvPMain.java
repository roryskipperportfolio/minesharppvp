package org.plugins.minesharppvp;

import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.minesharppvp.commands.MineSharpPvPCommand;
import org.plugins.minesharppvp.listeners.CombatTagListener;
import org.plugins.minesharppvp.listeners.EntityDamageEntityListener;
import org.plugins.minesharppvp.listeners.EntityShootBowListener;
import org.plugins.minesharppvp.listeners.PlayerChangedWorldListener;
import org.plugins.minesharppvp.listeners.PlayerCombustListener;
import org.plugins.minesharppvp.listeners.PlayerCommandListener;
import org.plugins.minesharppvp.listeners.PlayerDeathListener;
import org.plugins.minesharppvp.listeners.PlayerInteractListener;
import org.plugins.minesharppvp.listeners.PlayerItemConsumeListener;
import org.plugins.minesharppvp.listeners.PlayerJoinListener;
import org.plugins.minesharppvp.listeners.PlayerQuitListener;
import org.plugins.minesharppvp.listeners.PlayerTeleportListener;
import org.plugins.minesharppvp.managers.CombatLogManager;
import org.plugins.minesharppvp.managers.CommandManager;
import org.plugins.minesharppvp.managers.DeathManager;
import org.plugins.minesharppvp.managers.EssentialsManager;
import org.plugins.minesharppvp.managers.UnbanManager;
import org.plugins.minesharppvp.managers.SpawnpointManager;
import org.plugins.minesharppvp.util.ArenaConfig;
import org.plugins.minesharppvp.util.PlayersConfig;

public class MineSharpPvPMain extends JavaPlugin {

	private final UnbanManager unbanManager = new UnbanManager();
	private final CommandManager commandManager = new CommandManager();
	private final CombatLogManager combatLogManager = new CombatLogManager();
	private final DeathManager deathManager = new DeathManager();

	private EssentialsManager essentialsManager;
	private SpawnpointManager spawnpointManager;
	private ArenaConfig arenaConfig;
	private PlayersConfig playersConfig;

	@Override
	public void onEnable() {
		this.initalizeVariables();
		this.loadConfigs();
		this.registerCommands();
		this.registerListeners();
		this.combatLogManager.setupCombatLog();
		for (final Player p : Bukkit.getOnlinePlayers()) {
			if (p.getWorld().getName().equals(this.getConfig().getString("pvp-arena-options.worldname"))) {
				this.essentialsManager.disableGodmode(p);
				this.essentialsManager.disableFly(p);
				this.essentialsManager.resetSpeed(p);
			}
			if (this.getPlayersConfig().getConfig().isSet("temp-banned-players." + p.getUniqueId())) {
				if (System.currentTimeMillis() >= this.getPlayersConfig().getConfig().getLong("temp-banned-players." + p.getUniqueId() + ".unban-date")) {
					this.getPlayersConfig().getConfig().set("temp-banned-players." + p.getUniqueId(), null);
					this.getPlayersConfig().saveConfig();
					this.getPlayersConfig().reloadConfig();
					p.sendMessage(this.placeholders(this.getConfig().getString("messages.your-unbanned")));
					return;
				} else {
					this.unbanManager.addUnbanTask(p.getUniqueId(), new BukkitRunnable() {
						@Override
						public void run() {
							getPlayersConfig().getConfig().set("temp-banned-players." + p.getUniqueId(), null);
							getPlayersConfig().saveConfig();
							getPlayersConfig().reloadConfig();
							if (Bukkit.getPlayer(p.getUniqueId()) != null) {
								p.sendMessage(placeholders(getConfig().getString("messages.your-unbanned")));
							}
						}
					}.runTaskLater(this, 20L * ((this.getPlayersConfig().getConfig().getLong("temp-banned-players." + p.getUniqueId() + ".unban-date") - System.currentTimeMillis()) / 1000)));
				}
			}
		}
	}

	@Override
	public void onDisable() {
		this.unbanManager.cancelAllTasks();
	}

	private void initalizeVariables() {
		this.arenaConfig = new ArenaConfig(this);
		this.playersConfig = new PlayersConfig(this);
		this.spawnpointManager = new SpawnpointManager(this);
		this.essentialsManager = new EssentialsManager(this, this.spawnpointManager);
		this.spawnpointManager.loadSpawnpoints();
	}

	private void loadConfigs() {
		this.getConfig().options().copyDefaults(true);
		this.saveDefaultConfig();
		this.reloadConfig();

		this.arenaConfig.getConfig().options().copyDefaults(true);
		this.arenaConfig.saveDefaultConfig();
		this.arenaConfig.reloadConfig();

		this.playersConfig.getConfig().options().copyDefaults(true);
		this.playersConfig.saveDefaultConfig();
		this.playersConfig.reloadConfig();
	}

	private void registerCommands() {
		this.getCommand("minesharppvp").setExecutor(new MineSharpPvPCommand(this, this.spawnpointManager, this.unbanManager, this.deathManager));
	}

	private void registerListeners() {
		PluginManager plManager = this.getServer().getPluginManager();
		plManager.registerEvents(new CombatTagListener(this, this.commandManager), this);
		plManager.registerEvents(new EntityDamageEntityListener(this, this.spawnpointManager), this);
		plManager.registerEvents(new EntityShootBowListener(this, this.spawnpointManager), this);
		plManager.registerEvents(new PlayerChangedWorldListener(this, this.essentialsManager), this);
		plManager.registerEvents(new PlayerCombustListener(this.spawnpointManager), this);
		plManager.registerEvents(new PlayerCommandListener(this, this.commandManager, this.combatLogManager, this.essentialsManager), this);
		plManager.registerEvents(new PlayerDeathListener(this, this.deathManager), this);
		plManager.registerEvents(new PlayerInteractListener(this, this.spawnpointManager), this);
		plManager.registerEvents(new PlayerItemConsumeListener(this), this);
		plManager.registerEvents(new PlayerJoinListener(this, this.essentialsManager, this.unbanManager), this);
		plManager.registerEvents(new PlayerQuitListener(this.unbanManager, this.commandManager), this);
		plManager.registerEvents(new PlayerTeleportListener(this), this);
	}

	public ArenaConfig getArenaConfig() {
		return this.arenaConfig;
	}

	public PlayersConfig getPlayersConfig() {
		return this.playersConfig;
	}

	public String placeholders(String arg) {
		return StringEscapeUtils.unescapeJava(ChatColor.translateAlternateColorCodes('&', arg.replace("{PREFIX}", this.getConfig().getString("prefix"))));
	}
}
