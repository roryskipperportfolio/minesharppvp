package org.plugins.minesharppvp.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.EssentialsManager;
import org.plugins.minesharppvp.managers.UnbanManager;

public class PlayerJoinListener implements Listener {

	private final MineSharpPvPMain plugin;
	private final EssentialsManager essentialsManager;
	private final UnbanManager unbanManager;

	public PlayerJoinListener(MineSharpPvPMain plugin, EssentialsManager essentialsManager, UnbanManager unbanManager) {
		this.plugin = plugin;
		this.essentialsManager = essentialsManager;
		this.unbanManager = unbanManager;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player p = e.getPlayer();
		if (this.plugin.getPlayersConfig().getConfig().isSet("temp-banned-players." + p.getUniqueId())) {
			if (System.currentTimeMillis() >= this.plugin.getPlayersConfig().getConfig().getLong("temp-banned-players." + p.getUniqueId() + ".unban-date")) {
				this.plugin.getPlayersConfig().getConfig().set("temp-banned-players." + p.getUniqueId(), null);
				this.plugin.getPlayersConfig().saveConfig();
				this.plugin.getPlayersConfig().reloadConfig();
				new BukkitRunnable() {
					@Override
					public void run() {
							p.sendMessage(plugin.placeholders(plugin.getConfig().getString("messages.your-unbanned")));
					}
				}.runTaskLater(this.plugin, 21L);
			} else {
				this.unbanManager.addUnbanTask(p.getUniqueId(), new BukkitRunnable() {
					@Override
					public void run() {
						plugin.getPlayersConfig().getConfig().set("temp-banned-players." + p.getUniqueId(), null);
						plugin.getPlayersConfig().saveConfig();
						plugin.getPlayersConfig().reloadConfig();
						if (Bukkit.getPlayer(p.getUniqueId()) != null) {
							p.sendMessage(plugin.placeholders(plugin.getConfig().getString("messages.your-unbanned")));
						}
					}
				}.runTaskLater(this.plugin, 20L * ((this.plugin.getPlayersConfig().getConfig().getLong("temp-banned-players." + p.getUniqueId() + ".unban-date") - System.currentTimeMillis()) / 1000)));
			}
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				if (plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(p.getWorld().getName())) {
					if (plugin.getPlayersConfig().getConfig().isSet("temp-banned-players." + p.getUniqueId()) || plugin.getPlayersConfig().getConfig().isSet("banned-players." + p.getUniqueId())) {
						p.damage(20.00);
					} else {
						essentialsManager.disableFly(p);
						essentialsManager.disableGodmode(p);
						essentialsManager.resetSpeed(p);
						essentialsManager.disablePowertools(p);
					}
				}
			}
		}.runTaskLater(this.plugin, 20L);

	}

}
