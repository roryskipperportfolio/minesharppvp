package org.plugins.minesharppvp.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.SpawnpointManager;

public class EntityShootBowListener implements Listener {

	private final MineSharpPvPMain plugin;
	private final SpawnpointManager spawnpointManager;

	public EntityShootBowListener(MineSharpPvPMain plugin, SpawnpointManager spawnpointManager) {
		this.plugin = plugin;
		this.spawnpointManager = spawnpointManager;
	}

	@EventHandler
	public void onBowShoot(EntityShootBowEvent e) {
		if (e.getEntity() instanceof Player) {
			final Player p = (Player) e.getEntity();
			if (this.spawnpointManager.inSpawnpoint(e.getEntity().getLocation())) {
				e.getEntity().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.damager-in-spawnpoint")));
				e.setCancelled(true);
				p.updateInventory();
			}
		}
	}

}
