package org.plugins.minesharppvp.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.DeathManager;

public class PlayerDeathListener implements Listener {

	private final MineSharpPvPMain plugin;
	private final DeathManager deathManager;

	public PlayerDeathListener(MineSharpPvPMain plugin, DeathManager deathManager) {
		this.plugin = plugin;
		this.deathManager = deathManager;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDeath(final PlayerDeathEvent e) {
		if (this.plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(e.getEntity().getWorld().getName())) {
			ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			SkullMeta skullMeta = (SkullMeta) item.getItemMeta();
			skullMeta.setOwner(e.getEntity().getName());
			item.setItemMeta(skullMeta);
			e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), item);
			if (deathManager.wasForceKilled(e.getEntity().getUniqueId())) {
				if (plugin.getPlayersConfig().getConfig().isSet("banned-players." + e.getEntity().getUniqueId())) {
					e.setDeathMessage(plugin.placeholders(plugin.getConfig().getString("ban-death-message").replace("{PLAYER}", e.getEntity().getName()).replace("{BANNER}",
							plugin.getPlayersConfig().getConfig().getString("banned-players." + e.getEntity().getUniqueId() + ".banner"))));
				} else if (plugin.getPlayersConfig().getConfig().isSet("temp-banned-players." + e.getEntity().getUniqueId())) {
					Long time = (plugin.getPlayersConfig().getConfig().getLong("temp-banned-players." + e.getEntity().getUniqueId() + ".unban-date")
							- plugin.getPlayersConfig().getConfig().getLong("temp-banned-players." + e.getEntity().getUniqueId() + ".ban-date")) / 1000;
					e.setDeathMessage(plugin.placeholders(plugin.getConfig().getString("temp-ban-death-message").replace("{PLAYER}", e.getEntity().getName())
							.replace("{BANNER}", plugin.getPlayersConfig().getConfig().getString("temp-banned-players." + e.getEntity().getUniqueId() + ".banner")).replace("{TIME}", formatTime(time))));
				}
				deathManager.removeKilled(e.getEntity().getUniqueId());
			}
		}
	}

	private String formatTime(long seconds) {
		String timeText = "";
		boolean firstTime = true;
		if (seconds % (60 * 60 * 24 * 365) >= 0) {
			int timecalc = (int) Math.floor(seconds / (60 * 60 * 24 * 365));
			seconds = seconds % (60 * 60 * 24 * 365);
			if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " year,";
				} else {
					timeText += String.valueOf(timecalc) + " years,";
				}
				firstTime = false;
			}
		}

		if (seconds % (30 * 24 * 60 * 60) >= 0) {
			int timecalc = (int) Math.floor(seconds / (30 * 24 * 60 * 60));
			seconds = seconds % (30 * 24 * 60 * 60);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " month,";
				} else {
					timeText += String.valueOf(timecalc) + " months,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " month,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " months,";
				}
			}
		}

		if (seconds % (60 * 60 * 24 * 7) >= 0) {
			int timecalc = (int) Math.floor(seconds / (60 * 60 * 24 * 7));
			seconds = seconds % (60 * 60 * 24 * 7);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " week,";
				} else {
					timeText += String.valueOf(timecalc) + " weeks,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " week,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " weeks,";
				}
			}
		}

		if (seconds % (60 * 60 * 24) >= 0) {
			int timecalc = (int) Math.floor(seconds / (60 * 60 * 24));
			seconds = seconds % (60 * 60 * 24);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " day,";
				} else {
					timeText += String.valueOf(timecalc) + " days,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " day,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " days,";
				}
			}
		}

		if (seconds % (60 * 60) >= 0) {
			int timecalc = (int) Math.floor(seconds / (60 * 60));
			seconds = seconds % (60 * 60);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " hour,";
				} else {
					timeText += String.valueOf(timecalc) + " hours,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " hour,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " hours,";
				}
			}
		}

		if (seconds % 60 >= 0) {
			int timecalc = (int) Math.floor(seconds / (60));
			seconds = seconds % (60);
			if (timecalc != 0 && firstTime) {
				if (timecalc == 1) {
					timeText += String.valueOf(timecalc) + " minute,";
				} else {
					timeText += String.valueOf(timecalc) + " minutes,";
				}
				firstTime = false;
			} else if (timecalc != 0) {
				if (timecalc == 1) {
					timeText += " " + String.valueOf(timecalc) + " minute,";
				} else {
					timeText += " " + String.valueOf(timecalc) + " minutes,";
				}
			}
		}

		if (seconds > 0 && firstTime) {
			if (seconds == 1) {
				timeText += String.valueOf(seconds) + " second,";
			} else {
				timeText += String.valueOf(seconds) + " seconds,";
			}
		} else if (seconds > 0) {
			if (seconds == 1) {
				timeText += " " + String.valueOf(seconds) + " second,";
			} else {
				timeText += " " + String.valueOf(seconds) + " seconds,";
			}
		}
		timeText = timeText.substring(0, timeText.length() - 1);
		int lastComma = timeText.lastIndexOf(",");
		if (lastComma != -1) {
			timeText = timeText.substring(0, lastComma) + " and " + timeText.substring(lastComma + 2, timeText.length());
		}
		return timeText;
	}

}
