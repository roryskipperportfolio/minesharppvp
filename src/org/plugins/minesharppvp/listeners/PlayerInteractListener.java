package org.plugins.minesharppvp.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.SpawnpointManager;

public class PlayerInteractListener implements Listener {

	private final MineSharpPvPMain plugin;
	private final SpawnpointManager spawnpointManager;

	public PlayerInteractListener(MineSharpPvPMain plugin, SpawnpointManager spawnpointManager) {
		this.plugin = plugin;
		this.spawnpointManager = spawnpointManager;
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onInteract(PlayerInteractEvent e) {
		if (this.plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(e.getPlayer().getWorld().getName())) {
			if (e.getPlayer().getInventory().getItemInMainHand() != null) {
				if (e.getPlayer().getInventory().getItemInMainHand().getType() != Material.AIR) {
					ItemStack item = e.getPlayer().getInventory().getItemInMainHand();
					Material type = item.getType();
					Action action = e.getAction();
					if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
						if (this.spawnpointManager.inSpawnpoint(e.getPlayer().getLocation())) {
							switch (type.name()) {
							case "SPLASH_POTION":
								e.setCancelled(true);
								e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
								e.getPlayer().updateInventory();
								return;
							case "LINGERING_POTION":
								e.setCancelled(true);
								e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
								e.getPlayer().updateInventory();
								return;
							case "EGG":
								e.setCancelled(true);
								e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
								e.getPlayer().updateInventory();
								return;
							case "SNOWBALL":
								e.setCancelled(true);
								e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
								e.getPlayer().updateInventory();
								return;
							case "FISHING_ROD":
								e.setCancelled(true);
								e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
								e.getPlayer().updateInventory();
								return;
							}
							if (e.getPlayer().getInventory().getItemInOffHand() != null) {
								if (e.getPlayer().getInventory().getItemInOffHand().getType() != Material.AIR) {
									switch (e.getPlayer().getInventory().getItemInOffHand().getType().name()) {
									case "SPLASH_POTION":
										e.setCancelled(true);
										e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
										e.getPlayer().updateInventory();
										return;
									case "LINGERING_POTION":
										e.setCancelled(true);
										e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
										e.getPlayer().updateInventory();
										return;
									case "EGG":
										e.setCancelled(true);
										e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
										e.getPlayer().updateInventory();
										return;
									case "SNOWBALL":
										e.setCancelled(true);
										e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
										e.getPlayer().updateInventory();
										return;
									case "FISHING_ROD":
										e.setCancelled(true);
										e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
										e.getPlayer().updateInventory();
										return;
									}
								}
							}
						}
					}
					if (type == Material.COMPASS) {
						if (action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK) {
							if (e.getPlayer().hasPermission("worldedit.navigation.jumpto.tool")) {
								e.setCancelled(true);
								e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
								return;
							}
						}
						if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
							if (e.getPlayer().hasPermission("worldedit.navigation.thru.tool")) {
								e.setCancelled(true);
								e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
							}
						}
						return;
					} else if (type == Material.ENDER_PEARL) {
						e.setCancelled(true);
						e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-interaction")));
						e.getPlayer().updateInventory();
					}
				}
			}
		}
	}

}
