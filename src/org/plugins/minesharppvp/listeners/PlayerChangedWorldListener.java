package org.plugins.minesharppvp.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.EssentialsManager;

public class PlayerChangedWorldListener implements Listener {

	private final MineSharpPvPMain plugin;
	private final EssentialsManager essentialsManager;
	
	public PlayerChangedWorldListener(MineSharpPvPMain plugin, EssentialsManager essentialsManager) {
		this.plugin = plugin;
		this.essentialsManager = essentialsManager;
	}
	
	@EventHandler
	public void onPlayerWorldChange(PlayerChangedWorldEvent e) {
		if (this.plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(e.getPlayer().getWorld().getName())) {
			this.essentialsManager.disableGodmode(e.getPlayer());
			this.essentialsManager.disableFly(e.getPlayer());
			this.essentialsManager.resetSpeed(e.getPlayer());
			this.essentialsManager.disablePowertools(e.getPlayer());
		}
	}
	
}
