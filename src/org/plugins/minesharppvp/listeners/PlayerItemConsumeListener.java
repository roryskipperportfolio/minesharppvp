package org.plugins.minesharppvp.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.plugins.minesharppvp.MineSharpPvPMain;

public class PlayerItemConsumeListener implements Listener {

	private final MineSharpPvPMain plugin;

	public PlayerItemConsumeListener(MineSharpPvPMain plugin) {
		this.plugin = plugin;
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onFood(PlayerItemConsumeEvent e) {
		if (this.plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(e.getPlayer().getWorld().getName()) && e.getItem().getType() == Material.CHORUS_FRUIT) {
			e.setCancelled(true);
			e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-fruit")));
			e.getPlayer().updateInventory();
		}
	}

}
