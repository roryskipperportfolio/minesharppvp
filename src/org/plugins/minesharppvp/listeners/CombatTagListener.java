package org.plugins.minesharppvp.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.CommandManager;

import me.iiSnipez.CombatLog.Events.PlayerTagEvent;

public class CombatTagListener implements Listener {

	private final MineSharpPvPMain plugin;
	private final CommandManager commandManager;

	public CombatTagListener(MineSharpPvPMain plugin, CommandManager commandManager) {
		this.plugin = plugin;
		this.commandManager = commandManager;
	}

	@EventHandler(ignoreCancelled = true)
	public void onCombatTag(PlayerTagEvent e) {
		if (!e.getDamagee().hasPermission("mspvp.bypass")) {
			if (this.commandManager.hasCommandTask(e.getDamagee().getUniqueId())) {
				String command = this.commandManager.getDelayedCommand(e.getDamagee().getUniqueId()).getCommand();
				for (String combatLogCommand : this.plugin.getConfig().getStringList("pvp-arena-options.combatlog-commands")) {
					if (command.concat(" ").toLowerCase().startsWith(combatLogCommand.concat(" ").toLowerCase())) {
						this.commandManager.cancelCommandTask(e.getDamagee().getUniqueId());
						this.commandManager.removeCommandTask(e.getDamagee().getUniqueId());
						e.getDamagee().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.command-cancelled").replace("{COMMAND}", command)));
					}
				}
				this.commandManager.cancelCommandTask(e.getDamagee().getUniqueId());
			}
		}
		if (!e.getDamager().hasPermission("mspvp.bypass")) {
			if (this.commandManager.hasCommandTask(e.getDamager().getUniqueId())) {
				String command = this.commandManager.getDelayedCommand(e.getDamager().getUniqueId()).getCommand();
				for (String combatLogCommand : this.plugin.getConfig().getStringList("pvp-arena-options.combatlog-commands")) {
					if (command.concat(" ").toLowerCase().startsWith(combatLogCommand.concat(" ").toLowerCase())) {
						this.commandManager.cancelCommandTask(e.getDamager().getUniqueId());
						this.commandManager.removeCommandTask(e.getDamager().getUniqueId());
						e.getDamager().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.command-cancelled").replace("{COMMAND}", command)));
					}
				}
			}
		}
	}

}
