package org.plugins.minesharppvp.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.SpawnpointManager;

public class EntityDamageEntityListener implements Listener {

	private final MineSharpPvPMain plugin;
	private final SpawnpointManager spawnpointManager;

	public EntityDamageEntityListener(MineSharpPvPMain plugin, SpawnpointManager spawnpointManager) {
		this.plugin = plugin;
		this.spawnpointManager = spawnpointManager;
	}

	@EventHandler
	public void onEntityDamageEntity(EntityDamageByEntityEvent e) {

		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if (this.spawnpointManager.inSpawnpoint(p.getLocation())) {
				e.setCancelled(true);
				if (e.getEntity() instanceof Player) {
					p.sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.damager-in-spawnpoint")));
				}
				return;
			}
		}

		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if (this.spawnpointManager.inSpawnpoint(p.getLocation())) {
				e.setCancelled(true);
				if (e.getDamager() instanceof Player) {
					e.getDamager().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.player-in-spawnpoint")));
				}
				return;
			}

		}
	}

}
