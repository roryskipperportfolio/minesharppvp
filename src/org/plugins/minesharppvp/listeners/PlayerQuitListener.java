package org.plugins.minesharppvp.listeners;

import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.plugins.minesharppvp.managers.CommandManager;
import org.plugins.minesharppvp.managers.UnbanManager;

public class PlayerQuitListener implements Listener {

	private final UnbanManager unbanManager;
	private final CommandManager commandManager;

	public PlayerQuitListener(UnbanManager unbanManager, CommandManager commandManager) {
		this.unbanManager = unbanManager;
		this.commandManager = commandManager;
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		UUID uuid = e.getPlayer().getUniqueId();
		if (this.unbanManager.hasUnbanTask(uuid)) {
			this.unbanManager.cancelUnbanTask(uuid);
			this.unbanManager.removeUnbanTask(uuid);
		}
		if (this.commandManager.hasCommandTask(uuid)) {
			this.commandManager.cancelCommandTask(uuid);
			this.commandManager.removeCommandTask(uuid);
		}
	}

}
