package org.plugins.minesharppvp.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.plugins.minesharppvp.managers.SpawnpointManager;

public class PlayerCombustListener implements Listener {

	private final SpawnpointManager spawnpointManager;

	public PlayerCombustListener(SpawnpointManager spawnpointManager) {
		this.spawnpointManager = spawnpointManager;
	}

	@EventHandler
	public void onCombust(EntityCombustEvent e) {
		if (e.getEntity() instanceof Player) {
			if (this.spawnpointManager.inSpawnpoint(e.getEntity().getLocation())) {
				e.setCancelled(true);
			}
		}
	}

}
