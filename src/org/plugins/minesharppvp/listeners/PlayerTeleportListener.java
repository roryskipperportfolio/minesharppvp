package org.plugins.minesharppvp.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.plugins.minesharppvp.MineSharpPvPMain;

public class PlayerTeleportListener implements Listener {

	private final MineSharpPvPMain plugin;

	public PlayerTeleportListener(MineSharpPvPMain plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerWorldChange(PlayerTeleportEvent e) {
		if (!e.getFrom().getWorld().equals(e.getTo().getWorld())) {
			if (this.plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(e.getTo().getWorld().getName())
					&& (this.plugin.getPlayersConfig().getConfig().isSet("banned-players." + e.getPlayer().getUniqueId())
							|| this.plugin.getPlayersConfig().getConfig().isSet("temp-banned-players." + e.getPlayer().getUniqueId()))) {
				e.setCancelled(true);
				e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.fail-goto-pvp")));
			}
		}
	}

}
