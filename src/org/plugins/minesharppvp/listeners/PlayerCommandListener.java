package org.plugins.minesharppvp.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.plugins.minesharppvp.MineSharpPvPMain;
import org.plugins.minesharppvp.managers.CombatLogManager;
import org.plugins.minesharppvp.managers.CommandManager;
import org.plugins.minesharppvp.managers.EssentialsManager;
import org.plugins.minesharppvp.util.DelayedCommand;

public class PlayerCommandListener implements Listener {

	private final MineSharpPvPMain plugin;
	private final CommandManager commandManager;
	private final CombatLogManager combatLogManager;
	private final EssentialsManager essentialsManager;

	public PlayerCommandListener(MineSharpPvPMain plugin, CommandManager commandManager, CombatLogManager combatLogManager, EssentialsManager essentialsManager) {
		this.plugin = plugin;
		this.commandManager = commandManager;
		this.combatLogManager = combatLogManager;
		this.essentialsManager = essentialsManager;
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void preCommand(final PlayerCommandPreprocessEvent e) {
		String msg = e.getMessage().toLowerCase();
		if (msg.equals("/back") || msg.equals("/eback") || msg.equals("/essentials:back") || msg.equals("/essentials:eback")) {
			if (this.essentialsManager.backLocationInPvPWorld(e.getPlayer())) {
				e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.back-command-cancelled").replace("{COMMAND}", msg)));
				e.setCancelled(true);
			}
		}
		if (this.plugin.getConfig().getStringList("pvp-arena-options.worldnames").contains(e.getPlayer().getWorld().getName())) {
			if (this.combatLogManager.isTagged(e.getPlayer())) {
				for (String combatLogCommand : this.plugin.getConfig().getStringList("pvp-arena-options.combatlog-commands")) {
					String concat = combatLogCommand.endsWith(":") ? "" : " ";
					if (msg.concat(" ").startsWith(combatLogCommand.toLowerCase().concat(concat))) {
						e.setCancelled(true);
						e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.combatlog-command").replace("{COMMAND}", e.getMessage().toLowerCase())));
						return;
					}
				}
			}
			for (String delayedCommand : this.plugin.getConfig().getStringList("pvp-arena-options.delayed-commands")) {
				String concat = delayedCommand.endsWith(":") ? "" : " ";
				if (e.getMessage().concat(" ").toLowerCase().startsWith(delayedCommand.concat(concat).toLowerCase())) {
					e.setCancelled(true);
					e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.command-delay").replace("{COMMAND}", e.getMessage().toLowerCase()).replace("{DELAY}",
							this.plugin.getConfig().getString("pvp-arena-options.delay"))));
					this.commandManager.addCommandTask(e.getPlayer().getUniqueId(), new DelayedCommand(e.getMessage(), new BukkitRunnable() {
						@Override
						public void run() {
							e.getPlayer().performCommand(e.getMessage().substring(1));
						}
					}.runTaskLater(this.plugin, (long) (20.0 * this.plugin.getConfig().getDouble("pvp-arena-options.delay")))));
					return;
				}
			}
			for (String blockedCommand : this.plugin.getConfig().getStringList("pvp-arena-options.blocked-commands")) {
				String concat = blockedCommand.endsWith(":") ? "" : " ";
				if (e.getMessage().concat(" ").toLowerCase().startsWith(blockedCommand.concat(concat).toLowerCase())) {
					e.setCancelled(true);
					e.getPlayer().sendMessage(this.plugin.placeholders(this.plugin.getConfig().getString("messages.blocked-command").replace("{COMMAND}", e.getMessage().toLowerCase())));
					return;
				}
			}
		}
	}

}
