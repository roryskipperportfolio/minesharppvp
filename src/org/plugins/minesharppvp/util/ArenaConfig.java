package org.plugins.minesharppvp.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.plugins.minesharppvp.MineSharpPvPMain;

public class ArenaConfig {

	private MineSharpPvPMain plugin;
	private FileConfiguration arenaDataConfig = null;
	private File arenaDataConfigFile = null;

	public ArenaConfig(MineSharpPvPMain plugin) {
		this.plugin = plugin;
	}

	public void reloadConfig() {
		if (this.arenaDataConfigFile == null) {
			this.arenaDataConfigFile = new File(this.plugin.getDataFolder(), "arenadata.yml");
		}
		this.arenaDataConfig = YamlConfiguration.loadConfiguration(this.arenaDataConfigFile);

		// Look for defaults in the jar
		try {
			Reader defConfigStream = new InputStreamReader(this.plugin.getResource("arenadata.yml"), "UTF8");
			if (defConfigStream != null) {
				YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
				this.arenaDataConfig.setDefaults(defConfig);
				this.arenaDataConfig.options().copyDefaults(true);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public FileConfiguration getConfig() {
		if (this.arenaDataConfig == null) {
			reloadConfig();
		}
		return this.arenaDataConfig;
	}

	public void saveConfig() {
		if (this.arenaDataConfig == null || this.arenaDataConfigFile == null) {
			return;
		}
		try {
			getConfig().save(this.arenaDataConfigFile);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void saveDefaultConfig() {
		if (this.arenaDataConfigFile == null) {
			this.arenaDataConfigFile = new File(this.plugin.getDataFolder(), "arenadata.yml");
		}
		if (!this.arenaDataConfigFile.exists()) {
			this.plugin.saveResource("arenadata.yml", false);
		}
	}

}
