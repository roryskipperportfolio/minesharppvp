package org.plugins.minesharppvp.util;

import org.bukkit.scheduler.BukkitTask;

public class DelayedCommand {

	private final String command;
	private final BukkitTask task;
	
	public DelayedCommand(String command, BukkitTask task) {
		this.command = command;
		this.task = task;
	}
	
	public String getCommand() {
		return this.command;
	}
	
	public void cancel() {
		this.task.cancel();
	}
	
}
